# Spring Boot Microservice Archetype

## Creating a new project

To create a new project open you terminal and run the following command:

``` 
mvn archetype:generate \
  -DarchetypeGroupId=com.zenvia \
  -DarchetypeArtifactId=sms-spring-boot-microservice-archetype \
  -DarchetypeVersion=1.0.0 \
  -DgroupId=com.zenvia \
  -DartifactId=sms-chassis-poc \
  -DinteractiveMode=false 
```