def deployHlgScript = new File(request.getOutputDirectory(), request.getArtifactId() + "/deploy-hlg.sh")
deployHlgScript.setExecutable(true, false)

def deployPrdScript = new File(request.getOutputDirectory(), request.getArtifactId() + "/deploy-prd.sh")
deployPrdScript.setExecutable(true, false)