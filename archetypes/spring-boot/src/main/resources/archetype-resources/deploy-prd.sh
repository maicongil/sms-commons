#!/bin/sh
echo "== Login no nexus-push ==\n";
docker login nexus-push.zenvia.com;

APP_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
APP_NAME=$(mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout)

echo "== Construindo a imagem docker do $APP_NAME versão $APP_VERSION ==\n";
mvn spring-boot:build-image -DskipTests
docker push nexus-push.zenvia.com/zenvia-sms/$APP_NAME:$APP_VERSION;

echo "== Atualizando o cluster kubernetes com a versão $APP_VERSION ==\n";
sed "s/APP_VERSION/$APP_VERSION/g" k8s/k8s-resources-prd.yaml > k8s/k8s-resources-prd-$APP_VERSION.yaml;
kubectl --kubeconfig ${HOME}/.kube/config-prd-sms apply -f k8s/k8s-resources-prd-$APP_VERSION.yaml -n sms;
rm k8s/k8s-resources-prd-$APP_VERSION.yaml;

echo "== Deploy realizado ==\n";