# ${artifactId}

## Before Start

## Description
FILL THE PROJECT DESCRIPTION

## Requirements

* Java Runtime Environment Version 17+
* Maven Version 3.8+
* Docker
* Skaffold (Optional)
* K3d (Optional)

## Local Development

#### Using local k8s cluster

To be able to run the application in your local k8s cluster, we recommend 
two tools that will make this task a lot easier:

* [Skaffold](https://skaffold.dev/docs/install/) 
* [K3d](https://k3d.io/v5.4.1/) 

After installing the tools and creating your local cluster, you can start to use the skaffold commands to run your application in different modes:

Run the application in your local cluster:

``` skaffold run --port-forward ```

Run the application in your local cluster in debug mode: 

``` skaffold debug --port-forward ```

Run the application in your local cluster in debug mode:

``` skaffold dev --port-forward ```

# References

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.6.4/maven-plugin/reference/html/#build-image)

